package nl.about42.aoc.y2020

import scala.annotation.tailrec

object Day10 extends AOC2020App {
  val lines = asInts(getInputForDay("10")).sorted

  // we have to add 0 and max+3 to the list

  val all = lines.prepended(0).appended(lines.last + 3)
  val pairs = all.dropRight(1).zip(all.tail)

  val diffs = pairs.map(p => p._2 - p._1)

  val numOnes = diffs.count(_ == 1)
  val numTwos = diffs.count(_ == 2)
  val numThrees = diffs.count(_ == 3)

  println(s"There are $numOnes diffs of 1, $numTwos diffs of 2, and $numThrees diffs of 3 (incl device),\nproduct is ${numThrees * numOnes}")

  val max = all.last
  println(s"The min is ${all.head}, the max is $max")

  // find number of ways to reach the end

  val routeCount: Map[Int, Long] = process(all.tail, Map.empty + (0 -> 1L))

  println(s"There are ${routeCount.getOrElse(max, -1)} ways to reach the end")

  @tailrec
  def process(adapters: List[Int], knownRoutes: Map[Int, Long]): Map[Int, Long] = {
    val adapter = adapters.head
    val waysToReach: Long =
      (1 to 3).map(i => knownRoutes.get(adapter - i)).map(_.getOrElse(0L)).sum
    if (adapters.size == 1) {
      knownRoutes + (adapter -> waysToReach)
    } else {
      process(adapters.tail, knownRoutes + (adapter -> waysToReach))
    }
  }

}
