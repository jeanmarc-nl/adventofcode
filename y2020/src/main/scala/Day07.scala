package nl.about42.aoc.y2020

object Day07 extends AOC2020App {
  case class BagCount(bagColor: String, count: Int)

  case class BagRule(bagColor: String, content: Set[BagCount])

  val lines = getInputForDay("07")

  val rules = buildRules(lines)
  println(s"We have ${rules.size} rules")

  // now we build a list of bag colors that can hold shiny gold bags (directly or indirectly)
  // whenever a new color is detected, look for it if it has not yet been processed.
  // when the list of colors to check is empty, we're done

  var allColorsLeadingToShinyGold: Set[String] = Set.empty
  var checkedColors = Set("shiny gold")

  var colorsToProcess = findBagsForColor("shiny gold")

  while (colorsToProcess.nonEmpty) {
    val col = colorsToProcess.head
    allColorsLeadingToShinyGold = allColorsLeadingToShinyGold + col
    checkedColors = checkedColors + col
    val newColors = findBagsForColor(col).filter(!checkedColors.contains(_))
    colorsToProcess = colorsToProcess.tail ++ newColors
  }

  println(s"We detected ${allColorsLeadingToShinyGold.size} colors that can contain a shiny gold bag")

  println(s"A shiny gold bag must contain ${countBags("shiny gold")} bags")

  // now we look how many bags must be in a shiny gold bag
  def countBags(color: String): Int = {
    val bagRule = rules(color)
    bagRule.content.foldLeft(0)((acc, bagCount) =>
      acc + bagCount.count * (1 + countBags(bagCount.bagColor))
    )
  }

  def buildRules(input: List[String], acc: Map[String, BagRule] = Map.empty): Map[String, BagRule] = {
    if (input.isEmpty) {
      acc
    } else {
      val result = parseRule(input.head)
      buildRules(input.tail, acc + (result.bagColor -> result))
    }
  }

  def parseRule(input: String): BagRule = {
    val parts = input.split(" bags contain ")
    val rules = parts(1).split(",")
    val result = rules.flatMap(r => {
      if (r == "no other bags.") {
        None
      } else {
        val ruleParts = r.trim.split(" ")
        Some(BagCount(ruleParts(1) + " " + ruleParts(2), ruleParts(0).toInt))
      }
    })
    BagRule(parts(0), result.toSet)
  }

  def findBagsForColor(value: String): List[String] = {
    rules
      .filter(r => r._2.content.map(_.bagColor).contains(value))
      .map(_._1)
      .toList
  }

}
