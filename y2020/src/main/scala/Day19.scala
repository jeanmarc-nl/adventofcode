package nl.about42.aoc.y2020

import scala.collection.mutable

object Day19 extends AOC2020App {
  val input = getInputForDay("19")

  val (rawRules, rawData) = input.partition(_.contains(":"))
  var freeId = rawRules.size

  val rules = rawRules.flatMap(buildRule)
  rules.foreach(println)

  val ruleMap = mutable.Map[Int, Rule]()
  val literalMap = mutable.Map[Int, Literal]()
  rules.foreach(r => ruleMap(r.id) = r)
  rules.foreach {
    case Literal(id, lit) => literalMap(id) = Literal(id, lit)
    case _                => // no action
  }

  // simplify the rules as much as possible

  var done = false
  while (!done) {
    println(s"Iteration start - ${literalMap.size} literals")
    val litCount = literalMap.size
//    ruleMap.keys.foreach(k => {
//      ruleMap(k) match {
//        case
//      }
//    })
    done = (literalMap.size == litCount)
  }

  println(s"$literalMap")
  ruleMap.keys.toList.sorted.foreach(k => println(s"${ruleMap(k)}"))

  sealed trait Rule {
    def id: Int
  }

  case class Literal(id: Int, lit: String) extends Rule
  case class Combine(id: Int, ids: List[Int]) extends Rule
  case class Or(id: Int, left: Int, right: Int) extends Rule

  def buildRule(in: String): List[Rule] = {
    in match {
      case s"""$id: "a"""" => List(Literal(id.trim.toInt, "a"))
      case s"""$id: "b"""" => List(Literal(id.trim.toInt, "b"))
      case s"$id: ${l1} ${l2} | ${r1} ${r2}" => {
        val left = Combine(freeId, List(l1.toInt, l2.toInt))
        freeId += 1
        val right = Combine(freeId, List(r1.toInt, r2.toInt))
        freeId += 1
        List(Or(id.trim.toInt, left.id, right.id), left, right)
      }
      case s"$id: ${l} | ${r}" => List(Or(id.trim.toInt, l.toInt, r.toInt))
      case s"$id: $l $r"       => List(Combine(id.trim.toInt, List(l.toInt, r.toInt)))
      case s"$id: $idx"        => List(Combine(id.trim.toInt, List(idx.toInt)))
    }
  }

  def findCombine(indices: List[Int]): Option[Int] = {
    val rule = rules.find {
      case Combine(_, list) => list.size == indices.size && list.zip(indices).count(lr => lr._1 != lr._2) == 0
      case _                => false
    }
    rule match {
      case Some(rule) => Some(rule.id)
      case _          => None
    }
  }
}
