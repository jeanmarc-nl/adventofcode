package nl.about42.aoc.y2020

object Day13 extends AOC2020App {
  val input = getInputForDay("13")

  val timestamp = input.head.toInt
  val busIds = input.tail.head.split(',').filter(_ != "x").map(_.toInt).toList.sorted

  println(s"Timestamp $timestamp, bus IDs: ${busIds.mkString(",")}")

  val waitingTimes = busIds.map(id => (id, id - (timestamp % id), id * (id - (timestamp % id)))).sortBy(_._2)

  println(s"Waiting times: $waitingTimes")
  println(s"Shortest wait: ${waitingTimes.head}")

  val busIdsWithOffset = input.tail.head.split(',').toList.zipWithIndex.filter(_._1 != "x").map(in => (in._1.toInt, in._2)).sortBy(_._1).reverse
  //val busIdsWithOffset = List((7, 0), (13, 1), (59, 4), (31, 6), (19, 7)).sortBy(_._1).reverse
  //val busIdsWithOffset = List((67, 0), (7, 2), (59, 3), (61, 4)).sortBy(_._1).reverse
  //val busIdsWithOffset = List((7, 0), (13, 1), (59, 4), (31, 6), (19, 7)).sortBy(_._1).reverse
  //val busIdsWithOffset = List((1789, 0), (37, 1), (47, 2), (1889, 3)).sortBy(_._1).reverse
  println(busIdsWithOffset)

  // start search in increments of the highest busId, check if the other ids have the correct period
  val stepSize = busIdsWithOffset.head._1

  // my input has max busId == 421, and it must depart 44 minutes after the first bus
  // at departure time of the first bus, the offset for bus 421  must be at 421 -/- 44 == 377

  val time = stepSize - busIdsWithOffset.head._2
  println(s"Start searching from time $time, in increments of $stepSize")
  val matchTime = findMatch(time, stepSize, busIdsWithOffset.tail)
  println(s"Found match at $matchTime")

  def findMatch(time: Long, step: Long, remainingBusIds: List[(Int, Int)]): Long = {
    println(s"Time = $time, step = $step, remaining $remainingBusIds")
    var matchTime = time
    val period = remainingBusIds.head._1
    val offset = remainingBusIds.head._2
    while ((matchTime + offset) % period != 0) {
      matchTime += step
    }
    if (remainingBusIds.length == 1) {
      matchTime
    } else {
      findMatch(matchTime, step * period, remainingBusIds.tail)
    }
  }
}
