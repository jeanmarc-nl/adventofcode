package nl.about42.aoc.y2020

object Day01 extends AOC2020App {
  val lines = getInputForDay("01")

  println(s"We have ${lines.size} lines")
  val amounts = asInts(lines)

  val Some((m1, m2)) =  findMatch(amounts.head, 2020, amounts.tail)
  println(s"Found $m1 and $m2, with sum  ${m1 + m2} and product ${ m1 * m2 }")

  val (a, b, c) =  findTripleMatch(amounts.head, amounts.tail.head, 2020, amounts.tail.tail)
  println(s"Found $a, $b, and $c, with sum  ${a + b + c} and product ${ a * b * c}")

  def findMatch(first: Int, target: Int, list: List[Int]): Option[(Int, Int)] = {
    if (list.contains(target - first)){
      Some((first, target - first))
    } else {
      if (list.nonEmpty) {
        findMatch(list.head, target, list.tail)
      } else {
        None
      }
    }
  }

  def findTripleMatch(first: Int, second: Int, target: Int, list: List[Int]): (Int, Int, Int) = {

    findMatch(second, target-first, list) match  {
      case Some(x) => (first, x._1, x._2)
      case None => findTripleMatch(second, list.head, target, list.tail)
    }
  }

}
