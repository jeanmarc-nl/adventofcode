package nl.about42.aoc.y2020

import scala.collection.mutable

object Day15 extends AOC2020App {
  val input = getInputForDay("15")

  val spokenNumbers = mutable.Map[Int, List[Int]]()

  val startNumbers = input.head.split(',').map(_.toInt)
  startNumbers.zipWithIndex.foreach(num => spokenNumbers(num._1) = List(num._2 + 1))
  println(s"$spokenNumbers")
  var latestNumber = startNumbers.last
  (startNumbers.length + 1 to 30000000).foreach(turn => {
    spokenNumbers.get(latestNumber) match {
      case None => latestNumber = 0
      case Some(turns) if (turns.length == 1) => latestNumber = 0
      case Some(turns) =>
        latestNumber = turns.last - turns.head
    }
    if (turn % 1000000 == 0) println(s"Turn $turn number is $latestNumber")
    spokenNumbers(latestNumber) = (spokenNumbers.getOrElse(latestNumber, List.empty) :+ turn).takeRight(2)
  })
  println(s"The latest is $latestNumber")
}
