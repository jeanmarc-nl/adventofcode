package nl.about42.aoc.y2020

object Day12 extends AOC2020App {

  case class Action(key: Char, amount: Int)

  case class Position(x: Int, y: Int, direction: Char) {
    def doStep(action: Action): Position = {
      action match {
        case Action('F', amount) => move(this.direction, amount)
        case Action('R', angle) => rotate('R', angle)
        case Action('L', angle) => rotate('L', angle)
        case Action(direction, amount) => move(direction, amount)
      }
    }

    def rotate(direction: Char, degrees: Int): Position = {
      val orientations = "NESWNESWNESW"
      val delta = (degrees / 90) * (if (direction == 'R') 1 else -1)
      val current = 4 + "NESW".indexOf(this.direction)
      Position(this.x, this.y, orientations(current + delta))
    }

    def move(angle: Char, amount: Int): Position = {
      angle match {
        case 'N' => Position(this.x, this.y + amount, this.direction)
        case 'E' => Position(this.x + amount, this.y, this.direction)
        case 'S' => Position(this.x, this.y - amount, this.direction)
        case 'W' => Position(this.x - amount, this.y, this.direction)
      }
    }
  }

  case class PositionWithWaypoint(x: Int, y: Int, wpx: Int, wpy: Int) {
    def doStep(action: Action): PositionWithWaypoint = {
      action match {
        case Action('F', amount) => PositionWithWaypoint(this.x + amount * this.wpx, this.y + amount * this.wpy, this.wpx, this.wpy)
        case Action('R', angle) => doRotate('R', angle)
        case Action('L', angle) => doRotate('L', angle)
        case Action('N', amount) => this.copy(wpy = this.wpy + amount)
        case Action('E', amount) => this.copy(wpx = this.wpx + amount)
        case Action('S', amount) => this.copy(wpy = this.wpy - amount)
        case Action('W', amount) => this.copy(wpx = this.wpx - amount)
      }
    }

    def doRotate(direction: Char, angle: Int): PositionWithWaypoint = {
      (direction, angle) match {
        case ('R', 90) => PositionWithWaypoint(this.x, this.y, this.wpy, this.wpx * -1)
        case ('R', 180) => PositionWithWaypoint(this.x, this.y, this.wpx * -1, this.wpy * -1)
        case ('R', 270) => PositionWithWaypoint(this.x, this.y, this.wpy * -1, this.wpx)
        case ('L', 90) => PositionWithWaypoint(this.x, this.y, this.wpy * -1, this.wpx)
        case ('L', 180) => PositionWithWaypoint(this.x, this.y, this.wpx * -1, this.wpy * -1)
        case ('L', 270) => PositionWithWaypoint(this.x, this.y, this.wpy, this.wpx * -1)
      }
    }
  }

  val input = parse(getInputForDay("12"))

  val turns = input.filter(i => i.key == 'R' || i.key == 'L').map(_.amount).groupBy(identity).view.mapValues(_.size).toMap
  println(turns)
  // shows we only rotate in 90 degree increments

  val start = Position(0, 0, 'E')
  println(s"From $start a L90 gives ${start.doStep(Action('L', 90))}")

  val end = input.foldLeft[Position](start)((pos, step) => pos.doStep(step))
  println(s"Journey leads to $end")
  println(s"Manhattan distance is ${Math.abs(end.y) + Math.abs(end.x)}")

  val wpStart = PositionWithWaypoint(0, 0, 10, 1)
  println(s"From $wpStart a L90 gives ${wpStart.doStep(Action('L', 90))}")

  //val wpInput = parse(List("F10",    "N3",    "F7",    "R90",    "F11"  ))
  val wpInput = input

  val wpEnd = wpInput.foldLeft[PositionWithWaypoint](wpStart)((pos, step) => pos.doStep(step))
  println(s"Journey leads to $wpEnd")
  println(s"Manhattan distance is ${Math.abs(wpEnd.y) + Math.abs(wpEnd.x)}")

  def parse(input: List[String]): List[Action] = {
    input.map(in => {
      (in.head, in.tail.toInt)
    }).map(p => Action(p._1, p._2))
  }
}
