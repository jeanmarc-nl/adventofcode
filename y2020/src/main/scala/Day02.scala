package nl.about42.aoc.y2020

object Day02 extends AOC2020App {

  case class Entry(min: Int, max: Int, char: Char, pass: String)

  val lines = getInputForDay("02")

  println(s"We have ${lines.size} lines")

  val checkList = lines.map(parse)

  val passwordCount = checkList.map(isValidPassword).count(_ == true)
  val validCount = checkList.map(isValid).count(_ == true)

  println(s"There are ${passwordCount} valid passwords in the list of ${checkList.size} passwords")
  println(s"There are ${validCount} valid entries in the list of ${checkList.size} passwords")

  def isValidPassword(entry: Entry): Boolean = {
    val charCount = entry.pass.count(x => x == entry.char)
    entry.min <= charCount && charCount <= entry.max
  }

  def isValid(entry: Entry): Boolean = {
    val p1 = entry.pass(entry.min - 1)
    val p2 = entry.pass(entry.max - 1)

    (p1, p2) match {
      case (entry.char, entry.char) => false
      case (a, b) if (a != entry.char && b != entry.char) => false
      case _ => true
    }
  }

  def parse(in: String): Entry = {
    val parts = in.split(':')
    val policy = parts(0).split(' ')
    val sizes = policy(0).split('-')
    Entry(sizes(0).toInt, sizes(1).toInt, policy(1).head, parts(1).trim)
  }
}
