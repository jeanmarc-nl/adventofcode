package nl.about42.aoc.y2020

import scala.collection.mutable

object Day16 extends AOC2020App {
  case class FieldRange(name: String, r1: Range, r2: Range) {
    def isValid(value: Int): Boolean = {
      r1.contains(value) || r2.contains(value)
    }
  }

  val input = getInputForDay("16")

  val fieldRanges = parseRanges(input.take(20))
  val myTicket = input(22).split(',').map(_.toInt).toList

  val nearbyTickets = input.drop(25).map(_.split(',').map(_.toInt).toList)

  //  fieldRanges.foreach(println)
  //  nearbyTickets.foreach(println)

  var errorRate = 0
  var invalidCount = 0
  nearbyTickets.foreach(ticket => {
    // look for fields that are not valid for any of the field ranges
    val invalidFields = ticket.filter(field => fieldRanges.map(_.isValid(field)).count(_ == true) == 0)
    if (invalidFields.nonEmpty) {
      invalidCount += 1
    }
    errorRate += invalidFields.sum
  })
  println(s"Error rate is $errorRate")

  val validTickets = nearbyTickets.filter(ticket => !ticket.exists(field => fieldRanges.map(_.isValid(field)).count(_ == true) == 0))

  println(s"Out of ${nearbyTickets.length} tickets we counted $invalidCount invalid tickets and now have ${validTickets.length} remaining tickets")

  val valuesPerCell = validTickets.transpose.zipWithIndex

  val fieldMap = mutable.Map[FieldRange, Int]()

  // for each list of values, determine which FieldRanges qualify for all of them
  val valuesWithCandidates = valuesPerCell.map(vpc => {
    val candidates = fieldRanges.filter(fr => vpc._1.count(v => !fr.isValid(v)) == 0)
    (vpc, candidates)
  })

  valuesWithCandidates.foreach(vwc => println(s"Field ${vwc._1._2} has ${vwc._2.length} candidates"))
  while (fieldMap.size != valuesWithCandidates.length) {
    valuesWithCandidates.foreach(vwc => {
      val remainingCandidates = vwc._2.filter(fr => !fieldMap.contains(fr))
      if (remainingCandidates.length == 1) {
        fieldMap(remainingCandidates.head) = vwc._1._2
      }
    })
  }
  val searchedFields = List("departure location", "departure station", "departure platform", "departure track", "departure date", "departure time")
    .map(k => fieldMap(rangeByName(k)))

  val product = searchedFields.map(idx => myTicket(idx).toLong).product

  println(s"Departure fields (${searchedFields}) product is $product")

  def rangeByName(key: String): FieldRange = {
    fieldRanges.filter(fr => fr.name == key).head
  }


  def parseRanges(lines: List[String]): List[FieldRange] = {
    lines.map {
      case s"$name: $a-$b or $c-$d" => FieldRange(name, a.toInt to b.toInt, c.toInt to d.toInt)
      case huh => println(s"Parse error on [$huh]")
        FieldRange("unknown", 0 to 0, 0 to 0)
    }
  }
}
