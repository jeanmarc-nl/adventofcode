package nl.about42.aoc.y2020

object Day03 extends AOC2020App {

  case class JourneyStep(pos: Int, isTree: Boolean)

  val lines = getInputForDay("03")

  val width = lines.head.length
  println(s"Got a forest of $width by ${lines.size}")

  var journey = travel(JourneyStep(0, false), lines.tail, 3, 1, Seq.empty)

  val treeCount = journey.map(_.isTree).count(x => x)

  println(s"We've hit ${treeCount} trees on our ${journey.size} step travel")

  val treeCounts = List(
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2)
  ).map(in => travel(JourneyStep(0, false), lines.tail, in._1, in._2, Seq.empty))
    .map(_.map(_.isTree).count(x => x).toLong)

  println(s"The five journeys have ${treeCounts.mkString(", ")} steps, which are multiplied to ${treeCounts.product}")

  def travel(start: JourneyStep, forest: List[String], right: Int, down: Int, journey: Seq[JourneyStep]): Seq[JourneyStep] = {
    if (forest.size > down - 1) {
      val nextStep = step(start.pos, right, down, forest)
      if (down == 2)
        travel(nextStep, forest.tail.tail, right, down, journey :+ nextStep)
      else
        travel(nextStep, forest.tail, right, down, journey :+ nextStep)
    } else {
      journey
    }
  }

  def step(start: Int, right: Int, down: Int, forest: List[String]): JourneyStep = {
    val newPos = (start + right) % width
    val landOnTree = forest(down - 1).charAt(newPos) == '#'
    val result = JourneyStep(newPos, landOnTree)
    result
  }
}
