package nl.about42.aoc.y2020

import scala.collection.mutable

object Day14 extends AOC2020App {
  val input = getInputForDay("14")

  val memory = mutable.Map[Long, Long]()
  val mem2 = mutable.Map[Long, Long]()

  var currentMask = ""

  input.foreach(line => {
    if (line.startsWith("mask")) {
      currentMask = line.drop(7)
    } else {
      val (addr, value) = parse(line)
      memory(addr) = applyMask(value, currentMask)
    }
  })

  val total = memory.values.sum

  println(s"Memory contents sum is $total")

  input.foreach(line => {
    if (line.startsWith("mask")) {
      currentMask = line.drop(7)
    } else {
      val (addr, value) = parse(line)
      applyMemMask(addr, currentMask).foreach(address => mem2(address) = value)
    }
  })

  val total2 = mem2.values.sum

  println(s"Memory contents sum is $total2")


  def applyMask(value: Long, mask: String): Long = {
    val bits = ("000000000000000000000000000000000000" + value.toBinaryString).takeRight(36)
    val result = bits.zip(mask).map(bit => if (bit._2 == 'X') bit._1 else bit._2).mkString
    //println(s"$bits\n$mask\n$result")
    BigInt(result, 2).toLong
  }

  def applyMemMask(addr: Long, mask: String): List[Long] = {
    val xCount = mask.count(_ == 'X') // we must generate 2^xCount addresses
    val numAddresses = Math.pow(2, xCount).toInt
    val addressBits = ("000000000000000000000000000000000000" + addr.toBinaryString).takeRight(36)
    (0 until numAddresses).map(i => {
      val bits = ("0000000000" + i.toBinaryString).takeRight(xCount)
      // replace the X chars in mask with the bits
      val floatingMask = replaceX(mask, bits)
      val res = addressBits.zip(mask).zip(floatingMask).map(bit => if (bit._1._2 == '0') bit._1._1 else if (bit._1._2 == '1') '1' else bit._2).mkString
      //println(s"$i - $addr $mask $bits $floatingMask $res")
      BigInt(res, 2).toLong
    }).toList
  }

  def replaceX(mask: String, bits: String, acc: String = ""): String = {
    val left = mask.take(mask.indexOf('X'))
    val right = mask.takeRight(mask.length - mask.indexOf('X') - 1)
    if (bits.length == 1) {
      acc + left + bits + right
    } else {
      replaceX(right, bits.tail, acc + left + bits.head)
    }
  }

  def parse(line: String): (Long, Long) = {
    line match {
      case s"mem[$addr] = $value" => (addr.toInt, value.toInt)
      case _ => println("parse error")
        (0L, 0L)
    }
  }
}
