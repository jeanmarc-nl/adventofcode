package nl.about42.aoc.y2020

import scala.util.parsing.combinator.RegexParsers

// using https://github.com/sim642/adventofcode/blob/master/src/main/scala/eu/sim642/adventofcode2020/Day18.scala as inspiration

object Day18 extends AOC2020App with RegexParsers {
  trait Expression {
    def getValue: Long
  }

  case class Add(left: Expression, right: Expression) extends Expression {
    override def getValue: Long = left.getValue + right.getValue
  }

  case class Mul(left: Expression, right: Expression) extends Expression {
    override def getValue: Long = left.getValue * right.getValue
  }

  case class Value(v: Long) extends Expression {
    override def getValue: Long = v
  }

  case class Parenthesis(contents: Expression) extends Expression {
    override def getValue: Long = contents.getValue
  }

  object Part1Parser {
    def simple: Parser[Expression] = (
      "\\d+".r ^^ { value => Value(value.toInt) }
        | "(" ~> expr <~ ")"
    )

    def op: Parser[(Expression, Expression) => Expression] = (
      "+" ^^^ Add.apply _
        | "*" ^^^ Mul.apply
    )

    def expr: Parser[Expression] = chainl1(simple, op)
  }

  object Part2Parser {
    def simple: Parser[Expression] = (
      "\\d+".r ^^ { value => Value(value.toInt) }
        | "(" ~> expr <~ ")"
    )

    def factor: Parser[Expression] = chainl1(simple, "+" ^^^ Add.apply _)

    def expr: Parser[Expression] = chainl1(factor, "*" ^^^ Mul.apply _)
  }

  def parseData(input: Iterable[String], mainParserFunction: Parser[Expression]) = input.map(parseAll(mainParserFunction, _)).map { case Success(r, _) => r }

  val testData = Seq(
    "2 * 3 + (4 * 5)",
    "5 + (8 * 3 + 9 + 3 * 4 * 3)"
  )
  val result1 = parseData(testData, Part1Parser.expr)
  result1.foreach(println)
  val result2 = parseData(testData, Part2Parser.expr)
  result2.foreach(println)

  val input = getInputForDay("18")
  val sum1 = parseData(input, Part1Parser.expr).map(_.getValue).sum
  println(s"Part 2 Sum is $sum1")
  val sum2 = parseData(input, Part2Parser.expr).map(_.getValue).sum
  println(s"Part 2 Sum is $sum2")

}
