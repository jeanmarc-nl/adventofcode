package nl.about42.aoc.y2020

import scala.collection.mutable

object Day17 extends AOC2020App {

  class Conway4DGrid(val min: (Int, Int, Int, Int), val max: (Int, Int, Int, Int), activeCells: Set[(Int, Int, Int, Int)]) {
    def show = {
      println(s"Area from ${min} to ${max}, with ${activeCells.size} active cells")
    }

    def nextGen: Conway4DGrid = {
      val newMin = (min._1 - 1, min._2 - 1, min._3 - 1, min._4 - 1)
      val newMax = (max._1 + 1, max._2 + 1, max._3 + 1, max._4 + 1)
      val newActiveCells = mutable.Set[(Int, Int, Int, Int)]()

      (newMin._1 to newMax._1).foreach(x => {
        (newMin._2 to newMax._2).foreach(y => {
          (newMin._3 to newMax._3).foreach(z => {
            (newMin._3 to newMax._3).foreach(w => {
              val neighbors = get4DNeighbors((x, y, z, w))
              val activeNeighbors = activeCells.intersect(neighbors.toSet).size
              if (activeCells.contains((x, y, z, w))) {
                if (activeNeighbors == 2 || activeNeighbors == 3) {
                  newActiveCells.add((x, y, z, w))
                }
              } else {
                if (activeNeighbors == 3) newActiveCells.add((x, y, z, w))
              }
            })
          })
        })
      })
      new Conway4DGrid(newMin, newMax, newActiveCells.toSet)
    }

  }

  class ConwayGridState(val min: (Int, Int, Int), val max: (Int, Int, Int), activeCells: Set[(Int, Int, Int)]) {
    def show = {
      println(s"Area from ${min} to ${max}, with ${activeCells.size} active cells")
    }

    def nextGen: ConwayGridState = {
      val newMin = (min._1 - 1, min._2 - 1, min._3 - 1)
      val newMax = (max._1 + 1, max._2 + 1, max._3 + 1)
      val newActiveCells = mutable.Set[(Int, Int, Int)]()

      (newMin._1 to newMax._1).foreach(x => {
        (newMin._2 to newMax._2).foreach(y => {
          (newMin._3 to newMax._3).foreach(z => {
            val neighbors = getNeighbors((x, y, z))
            val activeNeighbors = activeCells.intersect(neighbors.toSet).size
            if (activeCells.contains((x, y, z))) {
              if (activeNeighbors == 2 || activeNeighbors == 3) {
                newActiveCells.add((x, y, z))
              }
            } else {
              if (activeNeighbors == 3) newActiveCells.add((x, y, z))
            }
          })
        })
      })
      new ConwayGridState(newMin, newMax, newActiveCells.toSet)
    }

    def as4D = new Conway4DGrid((min._1, min._2, min._3, 0), (max._1, max._2, max._3, 0), activeCells.map(c => (c._1, c._2, c._3, 0)))
  }

  val input = getInputForDay("17")

  val gen0 = parseInput(input)

  println("Star 1:")
  gen0.show
  gen0.nextGen.show

  gen0.nextGen.nextGen.nextGen.nextGen.nextGen.nextGen.show

  println("Star 2:")
  val con4Dgen0 = gen0.as4D
  con4Dgen0.show
  con4Dgen0.nextGen.show

  con4Dgen0.nextGen.nextGen.nextGen.nextGen.nextGen.nextGen.show


  def getNeighbors(pos: (Int, Int, Int)): List[(Int, Int, Int)] = {
    List(
      (pos._1 - 1, pos._2 - 1, pos._3 - 1),
      (pos._1 - 1, pos._2 - 1, pos._3),
      (pos._1 - 1, pos._2 - 1, pos._3 + 1),
      (pos._1 - 1, pos._2, pos._3 - 1),
      (pos._1 - 1, pos._2, pos._3),
      (pos._1 - 1, pos._2, pos._3 + 1),
      (pos._1 - 1, pos._2 + 1, pos._3 - 1),
      (pos._1 - 1, pos._2 + 1, pos._3),
      (pos._1 - 1, pos._2 + 1, pos._3 + 1),
      (pos._1, pos._2 - 1, pos._3 - 1),
      (pos._1, pos._2 - 1, pos._3),
      (pos._1, pos._2 - 1, pos._3 + 1),
      (pos._1, pos._2, pos._3 - 1),
      (pos._1, pos._2, pos._3 + 1),
      (pos._1, pos._2 + 1, pos._3 - 1),
      (pos._1, pos._2 + 1, pos._3),
      (pos._1, pos._2 + 1, pos._3 + 1),
      (pos._1 + 1, pos._2 - 1, pos._3 - 1),
      (pos._1 + 1, pos._2 - 1, pos._3),
      (pos._1 + 1, pos._2 - 1, pos._3 + 1),
      (pos._1 + 1, pos._2, pos._3 - 1),
      (pos._1 + 1, pos._2, pos._3),
      (pos._1 + 1, pos._2, pos._3 + 1),
      (pos._1 + 1, pos._2 + 1, pos._3 - 1),
      (pos._1 + 1, pos._2 + 1, pos._3),
      (pos._1 + 1, pos._2 + 1, pos._3 + 1)
    )
  }

  def get4DNeighbors(pos: (Int, Int, Int, Int)): Set[(Int, Int, Int, Int)] = {
    (-1 to 1).flatMap(x => {
      (-1 to 1).flatMap(y => {
        (-1 to 1).flatMap(z => {
          (-1 to 1).map(w => {
            (pos._1 + x, pos._2 + y, pos._3 + z, pos._4 + w)
          })
        })
      })
    }).toSet - pos
  }

  def parseInput(input: List[String]): ConwayGridState = {
    val maxX = input.head.length - 1
    val maxY = input.length - 1

    val activeCells = mutable.Set[(Int, Int, Int)]()
    (0 to maxY).foreach(y => {
      (0 to maxX).foreach(x => {
        if (input(y)(x) == '#') activeCells.add((x, y, 0))
      })
    })

    new ConwayGridState((0, 0, 0), (maxX, maxY, 0), activeCells.toSet)
  }
}
