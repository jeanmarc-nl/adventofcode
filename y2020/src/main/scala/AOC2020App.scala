package nl.about42.aoc.y2020

import nl.about42.aoc.common.AOCApp

trait AOC2020App extends AOCApp{
  override val year: String = "2020"
  override val resourceDir: String = "y2020/src/main/resources"
}
