package nl.about42.aoc.y2017

import nl.about42.aoc.common.AOCApp

trait AOC2017App extends AOCApp {
  override val year: String = "2017"
  override val resourceDir = "y2017/src/main/resources"
}
