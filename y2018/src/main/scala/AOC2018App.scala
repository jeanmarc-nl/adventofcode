package nl.about42.aoc.y2018

import nl.about42.aoc.common.AOCApp

trait AOC2018App extends AOCApp {
  override val year: String = "2018"
  override val resourceDir = "y2018/src/main/resources"
}
