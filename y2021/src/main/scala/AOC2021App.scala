package nl.about42.aoc.y2021

import nl.about42.aoc.common.AOCApp

trait AOC2021App extends AOCApp {
  override val resourceDir = "y2021/src/main/resources"
}
