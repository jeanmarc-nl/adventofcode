package nl.about42.aoc.y2021

object Day05 extends AOC2021App {
  case class Line(x1: Int, y1: Int, x2: Int, y2: Int)

  val lines = getInputForDay("05")

  val data = lines
    .map(l => {
      l.split(" -> ").map(point => {
        point.split(',').map(_.toInt)
      })
    })
    .map(points => Line(points(0)(0), points(0)(1), points(1)(0), points(1)(1)))

  println(s"List is $data")

  // take only horizontal or vertical lines
  val hv = data.filter(l => l.x1 == l.x2 || l.y1 == l.y2)

  val xMax = hv.map(l => if (l.x1 > l.x2) l.x1 else l.x2).max
  val yMax = hv.map(l => if (l.y1 > l.y2) l.y1 else l.y2).max

  val plane = Array.ofDim[Int](yMax + 1, xMax + 1)

  hv.foreach(drawHVLine(plane, _))
  //showPlane(plane)

  // count cells with value above 2
  val count = plane.flatten.count(c => c > 1)
  println(s"There are $count cells of value 2 or more")

  val p2 = Array.ofDim[Int](yMax + 1, xMax + 1)

  data.foreach(l => {
    if (l.x1 == l.x2 || l.y1 == l.y2) {
      drawHVLine(p2, l)
    } else {
      drawDiag(p2, l)
    }
  })
  //showPlane(p2)
  val count2 = p2.flatten.count(c => c > 1)
  println(s"There are $count2 cells of value 2 or more including diagonal")

  def drawDiag(plane: Array[Array[Int]], line: Line) = {
    val xStep = if (line.x1 > line.x2) -1 else 1
    val yStep = if (line.y1 > line.y2) -1 else 1
    (0 to Math.abs(line.y1 - line.y2)).foreach(i => {
      val (y, x) = (line.y1 + i * yStep, line.x1 + i * xStep)
      plane(y).update(x, plane(y)(x) + 1)
    })
  }

  def drawHVLine(plane: Array[Array[Int]], line: Line) = {
    if (line.x1 == line.x2) {
      (Math.min(line.y1, line.y2) to Math.max(line.y1, line.y2)).foreach(y =>
        plane(y).update(line.x1, plane(y)(line.x1) + 1)
      )
    } else {
      (Math.min(line.x1, line.x2) to Math.max(line.x1, line.x2)).foreach(x =>
        plane(line.y1).update(x, plane(line.y1)(x) + 1)
      )
    }
  }

  def showPlane(plane: Array[Array[Int]]) = {
    plane.foreach(row => {
      row.foreach(cell => print(cell))
      println()
    })
    println()
  }

}
