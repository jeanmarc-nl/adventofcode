package nl.about42.aoc.y2021

object Day07 extends AOC2021App {
  val lines = getInputForDay("07")

  val data = asInts(lines.head)

  val minPos: Int = data.min
  val maxPos: Int = data.max

  val costs = (minPos to maxPos).map(p => {
    fuelCost(p, data)
  })
  val costs2 = (minPos to maxPos).map(p => {
    fuelCost2(p, data)
  })

  println(s"$minPos $maxPos - - ${costs.min}")
  println(s"$minPos $maxPos - - ${costs2.min}")

  def fuelCost(p: Int, data: List[Int]): Int = {
    data.map(c => (c - p).abs).sum
  }

  def fuelCost2(p: Int, data: List[Int]): Int = {
    data
      .map(c => {
        (0 to (c - p).abs).sum
      })
      .sum
  }
}
