package nl.about42.aoc.y2021

import scala.collection.mutable.ListBuffer

object Day11 extends AOC2021App {
  val lines = getInputForDay("11")

  val grid = lines.map(l => l.toList.map(_.asDigit).toArray).toArray

  var flashCount = 0
  var i = 0
  var flashes = 0
  val populationSize = grid.length * grid(0).length
  do {
    flashes = doStep(grid)
    i = i + 1
    println(s"Step $i had $flashes flashes, flashCount is $flashCount")

  } while (flashes != populationSize)

  def doStep(g: Array[Array[Int]]): Int = {
    val rows = g.length
    val cols = g(0).length
    val hasFlashed = ListBuffer[(Int, Int)]()

    // add 1
    g.indices.foreach(r => {
      g(0).indices.foreach(c => {
        g(r)(c) = g(r)(c) + 1
      })
    })

    // flash
    var done = false
    while (!done) {
      done = true
      g.indices.foreach(r => {
        g(0).indices.foreach(c => {
          if (g(r)(c) > 9 && !hasFlashed.contains((r, c))) {
            getNeighbors(r, c, rows, cols).foreach(n => {
              g(n._1)(n._2) = g(n._1)(n._2) + 1
            })
            hasFlashed.append((r, c))
            done = false
          }
        })
      })
    }

    // set energy level to 0 for all octopuses that have flashed
    g.indices.foreach(r => {
      g(0).indices.foreach(c => {
        if (g(r)(c) > 9) g(r)(c) = 0
      })
    })

    flashCount += hasFlashed.size
    hasFlashed.size
  }

  def getNeighbors(x: Int, y: Int, maxX: Int, maxY: Int): List[(Int, Int)] = {
    List(
      (x - 1, y - 1),
      (x - 1, y),
      (x - 1, y + 1),
      (x, y - 1),
      (x, y + 1),
      (x + 1, y - 1),
      (x + 1, y),
      (x + 1, y + 1)
    ).filter(e => e._1 >= 0 && e._1 < maxX && e._2 >= 0 && e._2 < maxY)
  }

}
