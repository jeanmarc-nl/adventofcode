package nl.about42.aoc.y2021

object Day25 extends AOC2021App {
  val lines = getInputForDay("25")

  val startGrid = lines.map(l => l.toArray).toArray
  val width = lines(0).length
  val height = lines.length
  var moveCount = -1

  val grids = Array.ofDim[Char](3, lines.size, lines(0).size)
  var currentGrid = 0
  startGrid.indices.foreach(y =>
    startGrid(0).indices.foreach(x => {
      grids(currentGrid)(y)(x) = startGrid(y)(x)
    })
  )

  showGrid(grids(currentGrid))
  var stepCount = 0
  while (moveCount != 0) {
    stepCount += 1
    print(s"Step $stepCount")
    moveCount = 0
    clearGrid(nextGrid(currentGrid))
    // try to move east
    grids(currentGrid).indices.foreach(y =>
      grids(currentGrid)(0).indices.foreach(x => {
        grids(currentGrid)(y)(x) match {
          case '>' =>
            val (nextX, nextY) = nextEast(x, y)
            grids(nextGrid(currentGrid))(nextY)(nextX) = '>'
            if (nextX != x) {
              moveCount += 1
            }
          case 'v' => grids(nextGrid(currentGrid))(y)(x) = 'v'
          case _ => // no action
        }
      })
    )
    // try to move south
    currentGrid = nextGrid(currentGrid)
    clearGrid(nextGrid(currentGrid))
    grids(currentGrid).indices.foreach(y =>
      grids(currentGrid)(0).indices.foreach(x => {
        grids(currentGrid)(y)(x) match {
          case 'v' =>
            val (nextX, nextY) = nextSouth(x, y)
            grids(nextGrid(currentGrid))(nextY)(nextX) = 'v'
            if (nextY != y) {
              moveCount += 1
            }
          case '>' => grids(nextGrid(currentGrid))(y)(x) = '>'
          case _ => // no action
        }
      })
    )
    currentGrid = nextGrid(currentGrid)
    println(s" had $moveCount moves")
    //showGrid(grids(currentGrid))
  }

  println(s"Done, we have done $stepCount steps")

  def nextEast(x: Int, y: Int): (Int, Int) = {
    if (grids(currentGrid)(y)(eastNeighbor(x)) == '.') {
      (eastNeighbor(x), y)
    } else {
      (x, y)
    }
  }

  def nextSouth(x: Int, y: Int): (Int, Int) = {
    if (grids(currentGrid)(southNeighbor(y))(x) == '.') {
      (x, southNeighbor(y))
    } else {
      (x, y)
    }
  }

  def clearGrid(idx: Int) = grids(idx).indices.foreach(y =>
    grids(idx)(0).indices.foreach(x => grids(idx)(y)(x) = '.')
  )

  def nextGrid(idx: Int) = (idx + 1) % 3

  def eastNeighbor(idx: Int) = (idx + 1) % width

  def southNeighbor(idx: Int) = (idx + 1) % height

}
