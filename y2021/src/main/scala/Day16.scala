package nl.about42.aoc.y2021

import scala.collection.mutable.ListBuffer

object Day16 extends AOC2021App {
  case class Packet(
                     version: Int,
                     typeId: Int,
                     digits: Option[List[String]],
                     value: Option[BigInt],
                     content: Option[ListBuffer[Packet]]
                   )

  val lines = getInputForDay("16")

  val input = lines.head.flatMap(toBin)

  val packets = ListBuffer[Packet]()

  getPackets(0, input, packets, input.length)

  val total = packets.map(getVersionTotal).sum

  val packetValue = getPacketValue(packets.head)

  println()
  println(s"version total is ${total}")
  println(
    s"value total is ${packetValue}, there are ${packets.length} top level packets"
  )

  // add all version numbers
  def getVersionTotal(start: Packet): Int = {
    val subVersions = start.content
      .map(ps => ps.map(p => getVersionTotal(p)))
      .getOrElse(List.empty)
      .sum
    start.version + subVersions
  }

  def getPacketValue(start: Packet): BigInt = {
    start.typeId match {
      case 0 => // sum
        start.content.getOrElse(ListBuffer.empty).map(getPacketValue).sum
      case 1 => // product
        start.content
          .getOrElse(ListBuffer.empty)
          .map(getPacketValue)
          .product
      case 2 => // min
        start.content.getOrElse(ListBuffer.empty).map(getPacketValue).min
      case 3 => // max
        start.content.getOrElse(ListBuffer.empty).map(getPacketValue).max
      case 4 => // literal
        start.value.get
      case 5 => // greater
        val packets = start.content.get
        if (packets.length > 2)
          println(
            s"warning - type 5 for a list of ${packets.length} sub-packets"
          )
        if (getPacketValue(packets.head) > getPacketValue(packets(1))) 1 else 0
      case 6 => // less
        val packets = start.content.get
        if (packets.length > 2)
          println(
            s"warning - type 6 for a list of ${packets.length} sub-packets"
          )
        if (getPacketValue(packets.head) < getPacketValue(packets(1))) 1 else 0
      case 7 => // equal
        val packets = start.content.get
        if (packets.length > 2)
          println(
            s"warning - type 7 for a list of ${packets.length} sub-packets"
          )
        if (getPacketValue(packets.head) == getPacketValue(packets(1))) 1 else 0
    }
  }

  def getPackets(
                  start: Int,
                  input: String,
                  packets: ListBuffer[Packet],
                  maxString: Int,
                  maxPackets: Int = Int.MaxValue
                ): Int = {
    val version = getValue(input.substring(start, start + 3)).toInt
    val pType =
      getValue(input.substring(start + 3, start + 6)).toInt
    val nextPos: Int = pType match {
      case 4 =>
        val digits = ListBuffer[String]()
        var digitStart = start + 6
        // take 5 bits until the group starts with a 0
        while (input(digitStart) == '1') {
          digits.append(input.substring(digitStart + 1, digitStart + 5))
          digitStart += 5
        }
        digits.append(input.substring(digitStart + 1, digitStart + 5))
        digitStart += 5
        val packet = Packet(
          version,
          pType,
          Some(digits.toList),
          Some(getValue(digits.mkString)),
          content = None
        )
        packets.append(
          packet
        )
        digitStart
      case id =>
        val lengthType = input(start + 6)
        val packet =
          Packet(version, id, None, None, Some(ListBuffer[Packet]()))
        if (lengthType == '0') {
          // 15 bits length
          val length =
            getValue(
              input.substring(start + 7, start + 7 + 15)
            ).toInt
          getPackets(
            start + 7 + 15,
            input,
            packet.content.get,
            start + 7 + 15 + length.toInt
          )
          packets.append(packet)
          start + 7 + 15 + length
        } else {
          // 11 bits packet count
          val packetCount =
            getValue(
              input.substring(start + 7, start + 7 + 11)
            ).toInt
          val packet =
            Packet(version, id, None, None, Some(ListBuffer[Packet]()))
          var subPos = start + 7 + 11
          (1 to packetCount).foreach(i => {
            println(s"getting sub-packet $i of $packetCount")
            subPos = getPackets(subPos, input, packet.content.get, maxString, 1)
          })
          packets.append(packet)
          subPos
        }
    }
    if (nextPos < maxString && maxPackets > 1) {
      if (allZero(input.substring(nextPos))) {
        maxString
      } else {
        getPackets(nextPos, input, packets, maxString, maxPackets - 1)
      }
    } else {
      nextPos
    }
  }

  def allZero(s: String) = !s.contains('1')

  def toBin(digit: Char) =
    ("000" + Integer.parseUnsignedInt(s"$digit", 16).toBinaryString)
      .takeRight(4)

  def getValue(in: String): BigInt = {
    var res: BigInt = 0
    var pow: BigInt = 1
    in.reverse.foreach(bit => {
      if (bit == '1') res += pow
      pow *= 2
    })
    res
  }
}
