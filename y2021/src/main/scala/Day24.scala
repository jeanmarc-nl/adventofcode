package nl.about42.aoc.y2021

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Day24 extends AOC2021App {

  object ShortCircuit {
    var calcCout = 0
    var perfCount = 0
    val knownStates = mutable.HashMap[(Long, Long, Long, Long, Long), Long]()

    def calc(w: Long, a: Long, b: Long, c: Long, z: Long): Long = {
      calcCout += 1
      knownStates.getOrElse((w, a, b, c, z), perform(w, a, b, c, z))
    }

    def perform(w: Long, a: Long, b: Long, c: Long, z: Long): Long = {
      perfCount += 1
      val x = (z % 26) + b
      val z2 = z / a
      val newX = if (x == w) 0 else 1
      val newY = newX * 25 + 1
      val newZ = newY * z2
      val y2 = (w + c) * newX
      knownStates((w, a, b, c, z)) = newZ + y2
      newZ + y2
    }

  }

  class Solver(val prog: List[String]) {
    val lines = prog.map(l => l.split(' ').tail.map(_.toInt))

    val solutions = ListBuffer[String]()

    def solve(
               z: Long = 0,
               acc: String = "",
               candidates: List[Int] = List(9, 8, 7, 6, 5, 4, 3, 2, 1)
             ): Unit = {
      val line = acc.length
      if (acc.length == 14) {
        if (z == 0) {
          solutions.append(acc)
        }
      } else {
        candidates.foreach(c => {
          if (lines(line)(0) == 1) {
            // push new value into z, allow all candidates
            solve(
              ShortCircuit
                .calc(c, lines(line)(0), lines(line)(1), lines(line)(2), z),
              acc + c
            )
          } else {
            // only use candidate if it leads to a zero change to z
            val nextZ = ShortCircuit
              .calc(c, lines(line)(0), lines(line)(1), lines(line)(2), z)
            if (nextZ == z / 26) {
              solve(nextZ, acc + c)
            }
          }
        })
      }
    }
  }

  val lines = getInputForDay("24")

  // extract short-circuit values
  val shortProg = ListBuffer[String]()

  (0 until 14).foreach(i => {
    val a = lines(i * 18 + 4).split(' ')(2).toLong
    val b = lines(i * 18 + 5).split(' ')(2).toLong
    val c = lines(i * 18 + 15).split(' ')(2).toLong
    shortProg.append(s"xxx $a $b $c")
  })

  println(s"Short prog:")
  shortProg.foreach(println)

  val solver = new Solver(shortProg.toList)
  solver.solve()

  println(s"ShortCircuit calc count = ${ShortCircuit.calcCout}, perf count = ${ShortCircuit.perfCount}")
  println(s"Done, there are ${solver.solutions.length} solutions")
  println(s"Biggest solution is  ${solver.solutions.max}")
  println(s"Smallest solution is ${solver.solutions.min}")

}
