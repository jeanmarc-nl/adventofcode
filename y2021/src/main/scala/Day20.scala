package nl.about42.aoc.y2021

object Day20 extends AOC2021App {
  val lines = getInputForDay("20")

  val algorithm = lines.head

  val imageData = lines.tail.tail

  println(s"$algorithm")
  println()
  //  imageData.foreach(println)
  //  println()

  val gen1Infinity = if (algorithm(0) == '#') "1" else "0"
  val gen2Infinity =
    if ((if (gen1Infinity == "1") algorithm(511) else algorithm(0)) == '#') "1"
    else "0"
  val infinityValues = Array(gen2Infinity, gen1Infinity)
  infinityValues.zipWithIndex.foreach(v => println(s"gen ${v._2} is ${v._1}"))

  var image = Array.ofDim[Int](imageData.length, imageData.head.length)

  // initialize current gen
  imageData.indices.foreach(y => {
    imageData.head.indices.foreach(x => {
      if (imageData(y)(x) == '#')
        image(y)(x) = 1
    })
  })
  //  showImage(image)
  //  println()

  (0 to 49).foreach(genCount => {
    val nextGen = Array.ofDim[Int](image.length + 2, image(0).length + 2)
    nextGen.indices.foreach(y => {
      nextGen(0).indices.foreach(x => {
        val cellIndex =
          getIndex(x - 1, y - 1, image(0).length, image.length, genCount)
        val imageValue = if (algorithm(cellIndex) == '#') 1 else 0
        nextGen(y)(x) = imageValue
      })
    })
    image = nextGen
  })
  //  showImage(image)
  //  println()

  // count the ones
  val total = image.map(row => row.sum).sum
  println(s"There are $total lights")

  def getIndex(x: Int, y: Int, maxX: Int, maxY: Int, genCount: Int): Int = {
    val cells = getCellData(x, y)

    val indexString = cells
      .map(pair =>
        if (pair._1 < 0 || pair._2 < 0 || pair._1 >= maxX || pair._2 >= maxY)
          infinityValues(genCount % 2)
        else image(pair._2)(pair._1).toString
      )
      .mkString
    Integer.parseInt(indexString, 2)
  }

  // negative indices as well as too large indices are handled later
  def getCellData(x: Int, y: Int): List[(Int, Int)] = {
    List(
      (x - 1, y - 1),
      (x, y - 1),
      (x + 1, y - 1),
      (x - 1, y),
      (x, y),
      (x + 1, y),
      (x - 1, y + 1),
      (x, y + 1),
      (x + 1, y + 1)
    )
  }

  def showImage(g: Array[Array[Int]]) = {
    g.foreach(r => {
      r.foreach(e => if (e == 0) print('.') else print('#'))
      println()
    })
  }

}
