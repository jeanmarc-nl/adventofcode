package nl.about42.aoc.y2021

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Day12 extends AOC2021App {
  sealed trait Cave {
    def name: String
  }

  case class LargeCave(name: String) extends Cave

  case class SmallCave(name: String) extends Cave

  case class Path(c1: Cave, c2: Cave) {
    def getOther(c: Cave) = if (c1 == c) c2 else c1

    def isConnected(c: Cave) = c1 == c || c2 == c
  }

  val lines = getInputForDay("12")

  val caves: mutable.Set[Cave] = mutable.Set.empty

  val connections: mutable.Set[Path] = mutable.Set.empty

  lines.foreach(l => {
    val newCaves = l
      .split('-')
      .map {
        case n if (n == n.toUpperCase) => LargeCave(n)
        case n => SmallCave(n)
      }
    caves += newCaves(0)
    caves += newCaves(1)
    connections += Path(newCaves(0), newCaves(1))
  })

  val start = SmallCave("start")
  val finish = SmallCave("end")

  val pathsSingleVisit = ListBuffer[List[Cave]]()
  val pathsOneSmallTwice = ListBuffer[List[Cave]]()

  findPathsSingleVisit(start, caves.toSet - start)

  println(s"Part 1: small caves visited at most once: ${pathsSingleVisit.size}")

  findPathsOneSmallTwice(start, caves.toSet - start, List(start))

  println(
    s"Part 2: at most 1 double visit to small caves: ${pathsOneSmallTwice.size}"
  )

  def findPathsSingleVisit(start: Cave, allowedCaves: Set[Cave], journey: List[Cave] = List.empty): Unit = {
    val canReach = getCaves(start, allowedCaves)
    canReach.foreach {
      case c if (c == finish) => pathsSingleVisit.append(journey.appended(c))
      case c: SmallCave =>
        findPathsSingleVisit(c, allowedCaves - c, journey.appended(c))
      case c: LargeCave =>
        findPathsSingleVisit(c, allowedCaves, journey.appended(c))
    }
  }

  def findPathsOneSmallTwice(
                              start: Cave,
                              allowedCaves: Set[Cave],
                              journey: List[Cave] = List.empty,
                              visitedSmallCaves: Set[SmallCave] = Set.empty, smallDoubleVisit: Boolean = false): Unit = {
    val canReach = getCaves(start, allowedCaves)

    canReach.foreach {
      case c if (c == finish) =>
        pathsOneSmallTwice.append(journey.appended(c))
      case c: LargeCave =>
        findPathsOneSmallTwice(
          c,
          allowedCaves,
          journey.appended(c),
          visitedSmallCaves,
          smallDoubleVisit
        )
      case c: SmallCave =>
        (smallDoubleVisit, visitedSmallCaves.contains(c)) match {
          case (true, true) =>
          // dead end, cannot revisit another small cave
          case (true, false) =>
            // first time to this cave is ok, but it cannot be revisited
            findPathsOneSmallTwice(
              c,
              allowedCaves - c,
              journey.appended(c),
              visitedSmallCaves + c,
              smallDoubleVisit
            )
          case (false, true) =>
            // second time to a small cave for the very first time
            findPathsOneSmallTwice(
              c,
              allowedCaves - c,
              journey.appended(c),
              visitedSmallCaves,
              true
            )
          case (false, false) =>
            // first time to this cave is ok, and it can be revisited
            findPathsOneSmallTwice(
              c,
              allowedCaves,
              journey.appended(c),
              visitedSmallCaves + c,
              smallDoubleVisit
            )
        }

    }
  }

  def getCaves(start: Cave, allowedCaves: Set[Cave]): Set[Cave] = {
    connections
      .filter(c =>
        c.isConnected(start) && allowedCaves.contains(c.getOther(start))
      )
      .map(p => {
        p.getOther(start)
      })
      .toSet
  }

  def showPath(path: List[Cave]): String = {
    path.map(c => s"${c.name},").mkString
  }
}
