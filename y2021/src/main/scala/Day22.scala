package nl.about42.aoc.y2021

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks.{break, breakable}

object Day22 extends AOC2021App {

  case class Cuboid(on: Boolean, min: Array[Int], max: Array[Int]) {
    def size: Long = {
      (this.max(0) - this.min(0) + 1L) * (this.max(1) - this.min(1) + 1L) * (this
        .max(2) - this.min(2) + 1L)
    }

    override def toString: String = {
      s"Cuboid($on,(${min(0)},${min(1)},${min(2)}),(${max(0)},${max(1)},${max(2)}))}"
    }

    def getRange(idx: Int): Range = {
      min(idx) to max(idx)
    }

    def getOverlapRange(idx: Int, cuboid: Cuboid): (Int, Int) = {
      (
        Math.max(this.min(idx), cuboid.min(idx)),
        Math.min(this.max(idx), cuboid.max(idx))
      )
    }

    // create up to 26 Cuboids that define the region of this minus the region of cuboid
    // return itself if there is no overlap,
    // return empty list if this is fully contained in cuboid
    def subtract(cuboid: Cuboid): List[Cuboid] = {
      def axisRanges(idx: Int): List[Option[(Int, Int)]] = {
        val left =
          if (this.min(idx) < cuboid.min(idx))
            Some((this.min(idx), Math.min(this.max(idx), cuboid.min(idx) - 1)))
          else None
        val middle =
          if (
            this.min(idx) <= cuboid.max(idx) && this.max(idx) >= cuboid.min(idx)
          )
            Some(this.getOverlapRange(idx, cuboid))
          else None
        val right =
          if (this.max(idx) > cuboid.max(idx))
            Some((Math.max(this.min(idx), cuboid.max(idx) + 1), this.max(idx)))
          else None
        List(left, middle, right)
      }

      if (!this.overlap(cuboid)) {
        //println("No overlap")
        return List(this)
      }

      // if `this` lies completely within cuboid, short-circuit to None
      if (
        this.min(0) >= cuboid.min(0) && this.max(0) <= cuboid.max(0) &&
          this.min(1) >= cuboid.min(1) && this.max(1) <= cuboid.max(1) &&
          this.min(2) >= cuboid.min(2) && this.max(2) <= cuboid.max(2)
      ) {
        //println("Fully contained")
        return List.empty
      }

      // we have overlap somewhere, explode the cube into at most 26 cuboids
      val parts = ListBuffer[Cuboid]()
      axisRanges(0).zipWithIndex.foreach { case (x, idX) =>
        axisRanges(1).zipWithIndex.foreach { case (y, idY) =>
          axisRanges(2).zipWithIndex.foreach { case (z, idZ) =>
            // do not include the center (if the center is non zero, it is the cuboid that needs to be subtracted)
            if (idX == 1 && idY == 1 && idZ == 1) {
              // do nothing
            } else {
              (x, y, z) match {
                case (Some(cx), Some(cy), Some(cz)) =>
                  // we have a valid cube
                  parts.append(
                    Cuboid(
                      this.on,
                      Array(cx._1, cy._1, cz._1),
                      Array(cx._2, cy._2, cz._2)
                    )
                  )
                case _ => // empty cell
              }
            }
          }
        }
      }
      parts.toList
    }

    def getOverlapCuboid(cuboid: Cuboid): Option[Cuboid] = {
      if (this.overlap(cuboid)) {
        val overlap =
          (0 to 2).map(idx => this.getOverlapRange(idx, cuboid)).toList
        val min = overlap.map(_._1).toArray
        val max = overlap.map(_._2).toArray
        Some(Cuboid(cuboid.on, min, max))
      } else {
        None
      }
    }

    def overlap(cuboid: Cuboid): Boolean = {
      (0 to 2)
        .map(idx => {
          this.min(idx) <= cuboid.max(idx) && this.max(idx) >= cuboid.min(idx)
        })
        .forall(b => b)
    }

  }

  val lines = getInputForDay("22")

  val data = lines.map(parseCuboid)

  doPart1(data)

  // part 2

  var currentGeneration: List[Cuboid] = List.empty
  val nextGen = mutable.ListBuffer[Cuboid]()
  data.foreach(cuboid => {
    if (!cuboid.on) {
      // see for which lit cubes we need to turn some lights off
      currentGeneration.foreach(litCube => {
        val res = litCube.subtract(cuboid)
        res.foreach(newC => nextGen.append(newC))
      })
    } else {
      // keep current generation and add new one
      currentGeneration.foreach(litCube => {
        nextGen.append(litCube)
      })
      nextGen.append(cuboid)
    }
    currentGeneration = nextGen.toList
    nextGen.clear()
  })

  // now we have a bunch of lit cubes, but they can be overlapping
  val total = currentGeneration.map(_.size).sum

  val counted = ListBuffer[Cuboid]()
  val toCount = mutable.Queue[Cuboid]()

  var litCount = currentGeneration.head.size
  counted.append(currentGeneration.head)

  toCount.addAll(currentGeneration.tail)

  print("Counting...")
  while (toCount.nonEmpty) {
    val cuboid = toCount.dequeue()
    // if the current cuboid has no overlap with any counted cubes, count it
    breakable {
      counted.foreach(litCube => {
        if (litCube.overlap(cuboid)) {
          val remainingCubes = cuboid.subtract(litCube)
          toCount.addAll(remainingCubes)
          break()
        }
      })
      // there was no overlap, count this one
      litCount += cuboid.size
      counted.append(cuboid)
    }
  }
  println()
  println(s"The total count is now $litCount")

  def parseCuboid(in: String): Cuboid = {
    def getCoords(coords: String): (Int, Int) = {
      val numbers = coords.split('=')(1).split("\\.\\.")
      (numbers(0).toInt, numbers(1).toInt)
    }

    val data = in.split(' ')
    val coords = data(1).split(',')
    val (x1, x2) = getCoords(coords(0))
    val (y1, y2) = getCoords(coords(1))
    val (z1, z2) = getCoords(coords(2))
    val min = Array(Math.min(x1, x2), Math.min(y1, y2), Math.min(z1, z2))
    val max = Array(Math.max(x1, x2), Math.max(y1, y2), Math.max(z1, z2))
    Cuboid(data(0)(1) == 'n', min, max)
  }

  def doPart1(data: List[Cuboid]) = {

    val reactorState = mutable.Map[(Int, Int, Int), Boolean]()

    val initCuboid = Cuboid(false, Array(-50, -50, -50), Array(50, 50, 50))
    data.foreach(cuboid => {
      // if it affects the init region
      if (initCuboid.overlap(cuboid)) {
        val overlap = initCuboid.getOverlapCuboid(cuboid).get
        overlap
          .getRange(0)
          .foreach(x => {
            overlap
              .getRange(1)
              .foreach(y => {
                overlap
                  .getRange(2)
                  .foreach(z => {
                    if (overlap.on) {
                      // turn on
                      reactorState((x, y, z)) = true
                    } else {
                      // turn off if we know it
                      if (reactorState.contains((x, y, z))) {
                        reactorState((x, y, z)) = false
                      }
                    }
                  })
              })
          })
      }
    })

    // count the number of 'on' cells
    val onCells = reactorState.values.count(b => b)
    println(s"Part 1: There are $onCells on")

  }

}
