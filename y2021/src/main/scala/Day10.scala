package nl.about42.aoc.y2021

import scala.collection.mutable.ListBuffer

object Day10 extends AOC2021App {
  val lines = getInputForDay("10")

  var errorCount = 0
  val completionScores = ListBuffer[Long]()

  val nonCorrupted = lines.filter(l => {
    val (valid, acc) = isValid(l)
    if (valid && acc.nonEmpty) {
      //println(s"valid but not complete $l - acc = ${acc} - ${getScore(acc)}")
      completionScores += getScore(acc)
    }
    valid
  })

  println(s"Part 1: Errorcount $errorCount")

  val scores = completionScores.sorted
  val middle = scores.size / 2
  println(s"Part 2: Middle = ${scores(middle)}")

  def getScore(str: String, acc: Long = 0): Long = {
    if (str.isEmpty) return acc

    getScore(str.tail, 5 * acc + completionScore(str.head))
  }

  def completionScore(c: Char): Int = c match {
    case '(' => 1
    case '[' => 2
    case '{' => 3
    case '<' => 4
  }

  def isValid(str: String, acc: String = ""): (Boolean, String) = {
    if (str.isEmpty) {
      return (true, acc)
    }

    if (isOpen(str.head)) return isValid(str.tail, str.head + acc)

    if (acc.isEmpty || !isMatch(acc.head, str.head)) {
      return (false, acc)
    }

    isValid(str.tail, acc.tail)
  }

  def isMatch(o: Char, c: Char) = {
    (o, c) match {
      case ('[', ']') => true
      case ('(', ')') => true
      case ('{', '}') => true
      case ('<', '>') => true
      case (_, close) =>
        errorCount += errorScore(close)
        false
    }
  }

  def errorScore(c: Char): Int = c match {
    case ')' => 3
    case ']' => 57
    case '}' => 1197
    case '>' => 25137
  }

  def isCloser(in: Char) = Set(']', ')', '}', '>').contains(in)

  def isOpen(in: Char) = Set('[', '(', '{', '<').contains(in)
}
