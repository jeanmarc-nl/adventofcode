package nl.about42.aoc.y2021

object Day01 extends AOC2021App {
  val lines = getInputForDay("01")

  println(s"We have ${lines.size} lines")

  val data = lines.map(_.toInt).sliding(2).toList
  println(s"We have ${data.size} pairs")

  println(s"There are ${data.count(x => x(0) < x(1))} increases")

  val windows = lines.map(_.toInt).sliding(3).toList

  val windowSums = windows.map(l => l.sum)

  val windowPairs = windowSums.sliding(2)
  println(s"There are ${windowPairs.count(x => x(0) < x(1))} increases")
}
