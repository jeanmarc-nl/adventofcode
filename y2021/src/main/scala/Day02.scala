package nl.about42.aoc.y2021

object Day02 extends AOC2021App {
  object Direction {
    def apply(in: String): Direction = in.toLowerCase match {
      case "forward" => Forward
      case "down" => Down
      case "up" => Up
    }
  }

  sealed trait Direction

  case object Forward extends Direction

  case object Down extends Direction

  case object Up extends Direction

  object Step {
    def apply(in: String): Step = {
      val input = in.split(' ')
      (Direction(input(0)), input(1).toInt) match {
        case (Forward, x) => Step(Forward, x, 0)
        case (Up, y) => Step(Up, 0, -1 * y)
        case (Down, y) => Step(Down, 0, y)
      }
    }
  }

  case class Step(dir: Direction, x: Int, y: Int)

  case class Position(x: Int, y: Int, aim: Int)


  val lines = getInputForDay("02")

  val steps = lines.map(Step(_))

  val pos1 = getEndPos(Position(0, 0, 0), steps, doStep1)
  val pos2 = getEndPos(Position(0, 0, 0), steps, doStep2)

  println(s"${pos1} gives ${pos1.x * pos1.y}")
  println(s"${pos2} gives ${pos2.x * pos2.y}")

  def doStep1(pos: Position, step: Step): Position = {
    step match {
      case Step(Forward, x, _) =>
        Position(pos.x + x, pos.y, pos.aim)
      case Step(_, _, y) => Position(pos.x, pos.y + y, pos.aim)
    }
  }

  def doStep2(pos: Position, step: Step): Position = {
    step match {
      case Step(Forward, x, _) =>
        Position(pos.x + x, pos.y + x * pos.aim, pos.aim)
      case Step(_, _, y) => Position(pos.x, pos.y, pos.aim + y)
    }
  }

  def getEndPos(start: Position, steps: List[Step], proc: (Position, Step) => Position): Position = {
    steps match {
      case Nil => start
      case d :: tail => {
        getEndPos(proc(start, d), tail, proc)
      }
    }
  }

}
