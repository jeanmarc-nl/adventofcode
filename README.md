# Advent of Code
Here are my code solutions for the Advent of Code annual programming challenge.

2021 was my first attempt at participating in the live event. I have completed all puzzles in December (published in another
repository, with an accompanying site: https://jeanmarc-nl.bitbucket.io/aoc2021/index.html)

These original solutions are replicated in the current repo, and I am adding solutions for puzzles from earlier editions whenever I get to them.
