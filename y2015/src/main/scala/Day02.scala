package nl.about42.aoc.y2015

object Day02 extends AOC2015App {
  val input = getInputForDay("02")

  case class Box(l: Int, w: Int, h: Int) {
    def neededWrappingPaper: Int = {
      2 * l * w + 2 * w * h + 2 * l * h + Math.min(Math.min(l * w, w * h), h * l)
    }

    def neededRibbon: Int = {
      val smallestSides = List(l, w, h).sorted.take(2)
      smallestSides.map(_ * 2).sum + l * w * h
    }
  }

  val boxes = input.map { case s"${l}x${w}x${h}" => Box(l.toInt, w.toInt, h.toInt) }

  println(s"Part 1: ${boxes.map(_.neededWrappingPaper).sum}")
  println(s"Part 2: ${boxes.map(_.neededRibbon).sum}")
}
