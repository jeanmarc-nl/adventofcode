package nl.about42.aoc.y2015

import scala.collection.mutable

object Day03 extends AOC2015App {
  val input = getInputForDay("03")

  val houseMap = mutable.Map[(Int, Int), Int]()

  var (x, y) = (0, 0)

  houseMap((x, y)) = 1

  input.head.foreach { c =>
    c match {
      case '>' => x += 1
      case 'v' => y -= 1
      case '<' => x -= 1
      case '^' => y += 1
    }
    houseMap((x, y)) = houseMap.getOrElse((x, y), 0) + 1
  }
  println(s"Part 1: ${houseMap.keys.size}")

  // split the instructions between santa and robot
  val (a, b) = input.head.zipWithIndex.partition(ci => ci._2 % 2 == 0)

  val houseMap2 = mutable.Map[(Int, Int), Int]()
  x = 0
  y = 0
  process(a.map(_._1))
  x = 0
  y = 0
  process(b.map(_._1))
  println(s"Part 2: ${houseMap2.keys.size}")

  def process(route: IndexedSeq[Char]): Unit = {
    route.foreach { c =>
      c match {
        case '>' => x += 1
        case 'v' => y -= 1
        case '<' => x -= 1
        case '^' => y += 1
      }
      houseMap2((x, y)) = houseMap2.getOrElse((x, y), 0) + 1
    }
  }
}
