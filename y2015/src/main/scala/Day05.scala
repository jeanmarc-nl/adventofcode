package nl.about42.aoc.y2015

import scala.annotation.tailrec

object Day05 extends AOC2015App {
  val input = getInputForDay("05")

  val niceCount1 = input.count(isNicePart1)
  val niceCount2 = input.count(isNicePart2)

  println(s"Part 1: $niceCount1")
  println(s"Part 2: $niceCount2")

  def isNicePart1(in: String): Boolean = {
    hasVowels(in) && hasRepeat(in) && !hasIllegal(in)
  }

  def hasVowels(in: String): Boolean = {
    in.count("aeiou".contains(_)) >= 3
  }

  def hasRepeat(in: String): Boolean = {
    in.sliding(2).exists(s => s(0) == s(1))
  }

  def hasIllegal(in: String): Boolean = {
    val illegals = Set("ab", "cd", "pq", "xy")
    in.sliding(2).exists(illegals.contains)
  }

  def isNicePart2(in: String): Boolean = {
    hasPair(in) && hasRepeatWithOne(in)
  }

  def hasPair(in: String): Boolean = {
    val pairs = in.sliding(2).toList
    hasPair(pairs)
  }

  @tailrec
  def hasPair(in: List[String]): Boolean = {
    in match {
      case _ :: Nil => false
      case _ :: _ :: Nil => false
      case h1 :: h2 :: t => occurs(h1, t) || hasPair(h2 +: t)
    }
  }

  def occurs(p1: String, remainder: List[String]): Boolean = {
    remainder.contains(p1)
  }

  def hasRepeatWithOne(in: String): Boolean = {
    in.sliding(3).exists(s => s(0) == s(2))
  }
}
