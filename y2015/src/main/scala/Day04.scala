package nl.about42.aoc.y2015

import java.security.MessageDigest
object Day04 extends AOC2015App {
  val input = getInputForDay("04")

  var number: Long = 0

  var done = false
  val md5 = MessageDigest.getInstance("MD5")
  while (!done) {
    number += 1
    val s = s"${input.head}$number"
    done = md5.digest(s.getBytes("UTF-8")).take(3).map(b => "%02X" format b).mkString.take(5) == "00000"
  }
  println(s"Part 1: $number")

  done = false
  number -= 1
  while (!done) {
    number += 1
    val s = s"${input.head}$number"
    done = md5.digest(s.getBytes("UTF-8")).take(3).count(_ == 0) == 3
  }
  println(s"Part 2: $number")

}
