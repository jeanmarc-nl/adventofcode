package nl.about42.aoc.y2015

object Day08 extends AOC2015App {
  val input = getInputForDay("08")

  val codeSize = input.map(_.length).sum
  val memorySize = input.map(memSize).sum
  println(s"Part 1: $codeSize - $memorySize == ${codeSize - memorySize}")

  val encodeSize = input.map(encode).sum
  println(s"Part 2: $encodeSize - $codeSize == ${encodeSize - codeSize}")

  def memSize(in: String): Int = {
    in.replaceAll("""\\\\""", "A").replaceAll("""\\"""", "B").replaceAll("""\\x..""", "C").length - 2
  }

  def encode(in: String): Int = {
    in.map {
      case '"'  => 2
      case '\\' => 2
      case _    => 1
    }.sum + 2
  }
}
