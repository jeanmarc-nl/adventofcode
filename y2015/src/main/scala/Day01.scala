package nl.about42.aoc.y2015

import scala.annotation.tailrec

object Day01 extends AOC2015App {
  val input = getInputForDay("01")

  val part1 = input.head.map(c => {
    if (c == '(') 1 else -1
  }).sum
  println(s"Part 1: $part1")

  println(s"Part 2: ${findBasement(0, input.head)}")

  @tailrec
  def findBasement(floor: Int, route: String, pos: Int = 1): Int = {
    (route.head, route.tail) match {
      case (')', _) if floor == 0 => pos
      case (')', t) => findBasement(floor - 1, t, pos + 1)
      case (_, t) => findBasement(floor + 1, t, pos + 1)
    }
  }
}
