package nl.about42.aoc.y2015

import nl.about42.aoc.common.AOCApp

trait AOC2015App extends AOCApp {
  override val year: String = "2015"
  override val resourceDir = "y2015/src/main/resources"
}
