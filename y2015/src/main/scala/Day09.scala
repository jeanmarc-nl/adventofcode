package nl.about42.aoc.y2015

import scala.collection.mutable

object Day09 extends AOC2015App {
  val input = getInputForDay("09")

  val cities = mutable.Set[String]()
  val graph = mutable.Map[String, Int]()
  input.foreach { case s"$a to $b = $dist" =>
    val key = if (a < b) s"$a-$b" else s"$b-$a"
    graph(key) = dist.toInt
    cities += a
    cities += b
  }
  println(s"There are ${cities.size} cities and ${graph.size} connections")
  val cityIndices = cities.zipWithIndex.map(_.swap).toMap

  cityIndices.foreach(println)

  case class Route(distance: Int, cities: List[Int]) extends Ordered[Route] {
    override def compare(that: Route): Int = this.distance - that.distance
  }

  // prepare a priority list of possible routes (shortest first)
  val partialJourneys = mutable.PriorityQueue[Route]().reverse

  // journeys can start in any of the cities
  cityIndices.foreach(i => partialJourneys.enqueue(Route(0, List(i._1))))

  var shortestRoute = Int.MaxValue

  while (partialJourneys.nonEmpty) {
    val current = partialJourneys.dequeue()
    val candidates = getNextCities(current)
    if (candidates.isEmpty) {
      // we are a the end
      if (current.distance < shortestRoute) {
        shortestRoute = current.distance
        println(s"Found a shortest route so far: $current")
      }
    } else {
      candidates.foreach(c => {
        val newDistance = current.distance + graph(getDistanceKey(current.cities.last, c))
        // only consider partial route if it is shorter than the currently known shortest route
        if (newDistance < shortestRoute)
          partialJourneys.enqueue(Route(newDistance, current.cities :+ c))
      })
    }
  }
  println(s"Queue is empty, shortest route found is $shortestRoute")

  var longestRoute = 0
  val partialLongestJourneys = mutable.PriorityQueue[Route]()
  // journeys can start in any of the cities
  cityIndices.foreach(i => partialLongestJourneys.enqueue(Route(0, List(i._1))))
  while (partialLongestJourneys.nonEmpty) {
    val current = partialLongestJourneys.dequeue()
    val candidates = getNextCities(current)
    if (candidates.isEmpty) {
      // we are at the end
      if (current.distance > longestRoute) {
        longestRoute = current.distance
        println(s"Found a longest route so far: $current")
      }
    } else {
      candidates.foreach(c => {
        val newDistance = current.distance + graph(getDistanceKey(current.cities.last, c))
        partialLongestJourneys.enqueue(Route(newDistance, current.cities :+ c))
      })
    }
  }
  println(s"Queue is empty, longest route found is $longestRoute")

  def getNextCities(route: Route): List[Int] = {
    cityIndices.keys.filter(!route.cities.contains(_)).toList
  }

  def getDistanceKey(city1: Int, city2: Int): String = {
    val name1 = cityIndices(city1)
    val name2 = cityIndices(city2)
    if (name1 < name2) s"$name1-$name2" else s"$name2-$name1"
  }
}
