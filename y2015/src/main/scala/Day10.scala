package nl.about42.aoc.y2015

import scala.collection.mutable

object Day10 extends AOC2015App {
  val input = getInputForDay("10")

  //  // Naive implementation that constructs the string (works for 40, takes multiple hours for 50 and may not even complete)
  //  var maxAccum = 0
  //  var in = input.head
  //  (1 to 40).foreach(i => {
  //    in = process(in)
  //    println(s"$i max accum is $maxAccum. Step produces ${in.length}")
  //  })
  //
  //  @tailrec
  //  def process(in: String, out: String = "", acc: String = ""): String = {
  //    if (maxAccum < acc.length) maxAccum = acc.length
  //    if (in.isEmpty) {
  //      if (acc.isEmpty)
  //        out
  //      else
  //        s"$out${acc.length}${acc.head}"
  //    } else {
  //      (in.head, acc) match {
  //        case (c, "")                     => process(in.tail, out, s"$c")
  //        case (c, acc) if acc.contains(c) => process(in.tail, out, s"$acc$c")
  //        case (c, acc)                    => process(in.tail, s"$out${acc.length}${acc.head}", s"$c")
  //      }
  //    }
  //  }

  // part 1 and 2, using look-and-see info found on internet (a.o. http://www.se16.info/js/lands2.htm)
  case class Element(name: String, sequence: String)

  val elementMap = Map(
    "H" -> (Element("H", "22"), List("H")),
    "He" -> (Element("He", "13112221133211322112211213322112"), List("Hf", "Pa", "H", "Ca", "Li")),
    "Li" -> (Element("Li", "312211322212221121123222112"), List("He")),
    "Be" -> (Element("Be", "111312211312113221133211322112211213322112"), List("Ge", "Ca", "Li")),
    "B" -> (Element("B", "1321132122211322212221121123222112"), List("Be")),
    "C" -> (Element("C", "3113112211322112211213322112"), List("B")),
    "N" -> (Element("N", "111312212221121123222112"), List("C")),
    "O" -> (Element("O", "132112211213322112"), List("N")),
    "F" -> (Element("F", "31121123222112"), List("O")),
    "Ne" -> (Element("Ne", "111213322112"), List("F")),
    "Na" -> (Element("Na", "123222112"), List("Ne")),
    "Mg" -> (Element("Mg", "3113322112"), List("Pm", "Na")),
    "Al" -> (Element("Al", "1113222112"), List("Mg")),
    "Si" -> (Element("Si", "1322112"), List("Al")),
    "P" -> (Element("P", "311311222112"), List("Ho", "Si")),
    "S" -> (Element("S", "1113122112"), List("P")),
    "Cl" -> (Element("Cl", "132112"), List("S")),
    "Ar" -> (Element("Ar", "3112"), List("Cl")),
    "K" -> (Element("K", "1112"), List("Ar")),
    "Ca" -> (Element("Ca", "12"), List("K")),
    "Sc" -> (Element("Sc", "3113112221133112"), List("Ho", "Pa", "H", "Ca", "Co")),
    "Ti" -> (Element("Ti", "11131221131112"), List("Sc")),
    "V" -> (Element("V", "13211312"), List("Ti")),
    "Cr" -> (Element("Cr", "31132"), List("V")),
    "Mn" -> (Element("Mn", "111311222112"), List("Cr", "Si")),
    "Fe" -> (Element("Fe", "13122112"), List("Mn")),
    "Co" -> (Element("Co", "32112"), List("Fe")),
    "Ni" -> (Element("Ni", "11133112"), List("Zn", "Co")),
    "Cu" -> (Element("Cu", "131112"), List("Ni")),
    "Zn" -> (Element("Zn", "312"), List("Cu")),
    "Ga" -> (Element("Ga", "13221133122211332"), List("Eu", "Ca", "Ac", "H", "Ca", "Zn")),
    "Ge" -> (Element("Ge", "31131122211311122113222"), List("Ho", "Ga")),
    "As" -> (Element("As", "11131221131211322113322112"), List("Ge", "Na")),
    "Se" -> (Element("Se", "13211321222113222112"), List("As")),
    "Br" -> (Element("Br", "3113112211322112"), List("Se")),
    "Kr" -> (Element("Kr", "11131221222112"), List("Br")),
    "Rb" -> (Element("Rb", "1321122112"), List("Kr")),
    "Sr" -> (Element("Sr", "3112112"), List("Rb")),
    "Y" -> (Element("Y", "1112133"), List("Sr", "U")),
    "Zr" -> (Element("Zr", "12322211331222113112211"), List("Y", "H", "Ca", "Tc")),
    "Nb" -> (Element("Nb", "1113122113322113111221131221"), List("Er", "Zr")),
    "Mo" -> (Element("Mo", "13211322211312113211"), List("Nb")),
    "Tc" -> (Element("Tc", "311322113212221"), List("Mo")),
    "Ru" -> (Element("Ru", "132211331222113112211"), List("Eu", "Ca", "Tc")),
    "Rh" -> (Element("Rh", "311311222113111221131221"), List("Ho", "Ru")),
    "Pd" -> (Element("Pd", "111312211312113211"), List("Rh")),
    "Ag" -> (Element("Ag", "132113212221"), List("Pd")),
    "Cd" -> (Element("Cd", "3113112211"), List("Ag")),
    "In" -> (Element("In", "11131221"), List("Cd")),
    "Sn" -> (Element("Sn", "13211"), List("In")),
    "Sb" -> (Element("Sb", "3112221"), List("Pm", "Sn")),
    "Te" -> (Element("Te", "1322113312211"), List("Eu", "Ca", "Sb")),
    "I" -> (Element("I", "311311222113111221"), List("Ho", "Te")),
    "Xe" -> (Element("Xe", "11131221131211"), List("I")),
    "Cs" -> (Element("Cs", "13211321"), List("Xe")),
    "Ba" -> (Element("Ba", "311311"), List("Cs")),
    "La" -> (Element("La", "11131"), List("Ba")),
    "Ce" -> (Element("Ce", "1321133112"), List("La", "H", "Ca", "Co")),
    "Pr" -> (Element("Pr", "31131112"), List("Ce")),
    "Nd" -> (Element("Nd", "111312"), List("Pr")),
    "Pm" -> (Element("Pm", "132"), List("Nd")),
    "Sm" -> (Element("Sm", "311332"), List("Pm", "Ca", "Zn")),
    "Eu" -> (Element("Eu", "1113222"), List("Sm")),
    "Gd" -> (Element("Gd", "13221133112"), List("Eu", "Ca", "Co")),
    "Tb" -> (Element("Tb", "3113112221131112"), List("Ho", "Gd")),
    "Dy" -> (Element("Dy", "111312211312"), List("Tb")),
    "Ho" -> (Element("Ho", "1321132"), List("Dy")),
    "Er" -> (Element("Er", "311311222"), List("Ho", "Pm")),
    "Tm" -> (Element("Tm", "11131221133112"), List("Er", "Ca", "Co")),
    "Yb" -> (Element("Yb", "1321131112"), List("Tm")),
    "Lu" -> (Element("Lu", "311312"), List("Yb")),
    "Hf" -> (Element("Hf", "11132"), List("Lu")),
    "Ta" -> (Element("Ta", "13112221133211322112211213322113"), List("Hf", "Pa", "H", "Ca", "W")),
    "W" -> (Element("W", "312211322212221121123222113"), List("Ta")),
    "Re" -> (Element("Re", "111312211312113221133211322112211213322113"), List("Ge", "Ca", "W")),
    "Os" -> (Element("Os", "1321132122211322212221121123222113"), List("Re")),
    "Ir" -> (Element("Ir", "3113112211322112211213322113"), List("Os")),
    "Pt" -> (Element("Pt", "111312212221121123222113"), List("Ir")),
    "Au" -> (Element("Au", "132112211213322113"), List("Pt")),
    "Hg" -> (Element("Hg", "31121123222113"), List("Au")),
    "Tl" -> (Element("Tl", "111213322113"), List("Hg")),
    "Pb" -> (Element("Pb", "123222113"), List("Tl")),
    "Bi" -> (Element("Bi", "3113322113"), List("Pm", "Pb")),
    "Po" -> (Element("Po", "1113222113"), List("Bi")),
    "At" -> (Element("At", "1322113"), List("Po")),
    "Rn" -> (Element("Rn", "311311222113"), List("Ho", "At")),
    "Fr" -> (Element("Fr", "1113122113"), List("Rn")),
    "Ra" -> (Element("Ra", "132113"), List("Fr")),
    "Ac" -> (Element("Ac", "3113"), List("Ra")),
    "Th" -> (Element("Th", "1113"), List("Ac")),
    "Pa" -> (Element("Pa", "13"), List("Th")),
    "U" -> (Element("U", "3"), List("Pa"))
  )
  val seqToElem = elementMap.values.map(entry => entry._1.sequence -> entry._1.name).toMap
  val inputElement = seqToElem(input.head)
  println(s"We start with element $inputElement")

  // keep track of the size of an element after N production steps
  val knownProductions = mutable.Map[(String, Int), Int]()
  var sequence = List(elementMap(inputElement)._1)

  println(s"40 generations leads to ${getSize(inputElement, 40)}")
  println(s"50 generations leads to ${getSize(inputElement, 50)}")

  def getSize(element: String, generation: Int): Int = {
    if (generation == 0) {
      elementMap(element)._1.sequence.length
    } else {
      if (knownProductions.contains((element, generation))) {
        knownProductions((element, generation))
      } else {
        val result = elementMap(element)._2.map(getSize(_, generation - 1)).sum
        knownProductions((element, generation)) = result
        result
      }
    }
  }

}
