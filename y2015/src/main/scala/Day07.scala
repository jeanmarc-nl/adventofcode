package nl.about42.aoc.y2015

import scala.collection.mutable

object Day07 extends AOC2015App {

  object Gates extends Enumeration {
    type GateType = Value
    val And, Lshift, Not, Or, Rshift, Set = Value
  }

  import Day07.Gates._

  case class Gate(id: String, gateType: GateType, in1: String, in2: Option[String] = None, value: Option[Int] = None) {
    def getValue(map: Map[String, Gate], knownValues: mutable.Map[String, Int]): Int = {
      if (knownValues.contains(id))
        return knownValues(id)
      println(s"Working on $this")
      val result = gateType match {
        case And if in1 == "" => value.get & map(in2.get).getValue(map, knownValues)
        case And              => map(in1).getValue(map, knownValues) & map(in2.get).getValue(map, knownValues)
        case Lshift           => map(in1).getValue(map, knownValues) << value.get
        case Not              => ~map(in1).getValue(map, knownValues)
        case Or               => map(in1).getValue(map, knownValues) | map(in2.get).getValue(map, knownValues)
        case Rshift           => map(in1).getValue(map, knownValues) >> value.get
        case Set if in1 == "" => value.get
        case Set              => map(in1).getValue(map, knownValues)
      }
      println(s"$this gives $result")
      knownValues(id) = result
      result
    }
  }

  val input = getInputForDay("07")
  val gates = input.map {
    case s"NOT $in -> $id"                                        => Gate(id, Not, in)
    case s"$in1 OR $in2 -> $id"                                   => Gate(id, Or, in1, Some(in2))
    case s"$in1 AND $in2 -> $id" if in1.forall(Character.isDigit) => Gate(id, And, "", Some(in2), Some(in1.toInt))
    case s"$in1 AND $in2 -> $id"                                  => Gate(id, And, in1, Some(in2))
    case s"$in1 LSHIFT $in2 -> $id"                               => Gate(id, Lshift, in1, value = Some(in2.toInt))
    case s"$in1 RSHIFT $in2 -> $id"                               => Gate(id, Rshift, in1, value = Some(in2.toInt))
    case s"$in -> $id" if in.forall(Character.isDigit)            => Gate(id, Set, "", value = Some(in.toInt))
    case s"$in -> $id"                                            => Gate(id, Set, in)
    case hmm                                                      => println(s"No match for [$hmm]"); Gate("unknown", And, "unknown")
  }

  val gateMap = gates.map(g => (g.id, g)).toMap

  val knownValues = mutable.Map[String, Int]()

  val part1Solution = gateMap("a").getValue(gateMap, knownValues)
  println(s"Part 1: $part1Solution")

  val part2Map = gateMap + ("b" -> Gate("b", Set, "", value = Some(part1Solution)))
  val part2Values = mutable.Map[String, Int]()
  val part2Solution = part2Map("a").getValue(part2Map, part2Values)
  println(s"Part 2: $part2Solution")

}
