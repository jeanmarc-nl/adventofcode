package nl.about42.aoc.y2015

import Day06.Instructions.{Off, On, Operand, Toggle}

object Day06 extends AOC2015App {
  val input = getInputForDay("06")

  object Instructions extends Enumeration {
    type Operand = Value
    val On, Off, Toggle = Value
  }

  case class Instruction(op: Operand, x1: Int, y1: Int, x2: Int, y2: Int) {
    def isOrdered: Boolean = x1 <= x2 && y1 <= y2

    def process1(board: Array[Array[Boolean]]): Unit = {
      (x1 to x2).foreach(x => {
        (y1 to y2).foreach(y => {
          op match {
            case On => board(x)(y) = true
            case Off => board(x)(y) = false
            case Toggle => board(x)(y) = !board(x)(y)
          }
        })
      })
    }

    def process2(board: Array[Array[Int]]): Unit = {
      (x1 to x2).foreach(x => {
        (y1 to y2).foreach(y => {
          op match {
            case On => board(x)(y) += 1
            case Off => board(x)(y) = Math.max(board(x)(y) - 1, 0)
            case Toggle => board(x)(y) += 2
          }
        })
      })
    }
  }

  val instructions = input.map {
    case s"turn on $x1,$y1 through $x2,$y2" => Instruction(On, x1.toInt, y1.toInt, x2.toInt, y2.toInt)
    case s"turn off $x1,$y1 through $x2,$y2" => Instruction(Off, x1.toInt, y1.toInt, x2.toInt, y2.toInt)
    case s"toggle $x1,$y1 through $x2,$y2" => Instruction(Toggle, x1.toInt, y1.toInt, x2.toInt, y2.toInt)
  }
  println(s"There are ${instructions.length} instructions, of which ${instructions.count(_.isOrdered)} are ordered")

  val board = Array.ofDim[Boolean](1000, 1000)

  instructions.foreach(_.process1(board))

  println(s"Part 1: ${board.map(_.count(_ == true)).sum}")

  val board2 = Array.ofDim[Int](1000, 1000)
  instructions.foreach(_.process2(board2))
  println(s"Part 2: ${board2.map(_.sum).sum}")
}
