package nl.about42.aoc.common

import com.typesafe.config.ConfigFactory

import java.io.{File, FileWriter}
import java.net.{HttpURLConnection, URL}
import scala.io.Source
import scala.reflect.ClassTag
import scala.util.Using

trait AOCApp extends App with InputHandler

trait InputHandler {
  val resourceDir = "src/main/resources"
  val year = "2021"

  def getInputForDay(day: String, useCache: Boolean = true): List[String] = {
    if (useCache && exists(day.toInt)) {
      println(s"Getting $year day $day from local filesystem...")
      getInput(s"aoc${year}_${day}_input.txt")
    } else {
      if (useCache){
        println(s"Cache wanted, but not yet present, fetching...")
      }
      println(s"Getting $year day $day from AOC site...")
      // drop any leading zero
      val aocDay = day.toInt
      getInputFromUrl(aocDay)
    }
  }

  def getInputFromUrl(day: Int): List[String] = {

    val filename = cacheName(day)

    val config = ConfigFactory.load("connection.properties")
    val baseUrl = config.getString("baseUrl") + s"/$year"
    val cookie = config.getString("cookie")
    val connection = new URL(s"$baseUrl/day/$day/input").openConnection
      .asInstanceOf[HttpURLConnection]
    connection.setRequestProperty("cookie", s"session=$cookie")
    val result = cacheResult(
      Source.fromInputStream(connection.getInputStream).mkString,
      filename
    )
    println("closing connection")
    connection.getInputStream.close()
    connection.disconnect()
    result.split('\n').toList
  }

  private def cacheName(day: Int): String = {
    f"aoc${year}_${day}%02d_input.txt"
  }
  private def location(fileName: String): String ={
    s"${resourceDir}/$fileName"
  }

  private def exists(day: Int): Boolean = {
    val cacheLocation = new File(location(cacheName(day)))
    cacheLocation.exists() && cacheLocation.isFile
  }

  private def cacheResult(str: String, cacheFile: String): String = {
    val cacheLocation = new File(location(cacheFile))
    // make sure folders exist
    new File(cacheLocation.getParent).mkdirs()
    val fileWriter = new FileWriter(cacheLocation)
    println(s"Updating cache $cacheLocation...")
    fileWriter.write(str)
    fileWriter.close()
    str
  }

  def getInput(file: String): List[String] = {
    Using.resource(Source.fromFile(s"${location(file)}")) { source =>
      source.getLines().toList
    }
  }

  def asInts(in: String, splitter: String = ","): List[Int] =
    in.split(splitter).toList.map(_.trim.toInt)

  def asInts(in: List[String]): List[Int] = in.map(_.toInt)

  def intGrid(in: List[String]): Array[Array[Int]] =
    in.map(_.toArray.map(_.asDigit)).toArray

  def showGrid(g: Array[Array[Int]]) = {
    g.foreach(r => {
      r.foreach(e => print(f"$e%2d"))
      println()
    })
  }

  def showGrid[T](g: Array[Array[T]]) = {
    g.foreach(r => {
      r.foreach(e => print(e.toString))
      println()
    })
  }

  def transpose[T: ClassTag](in: Array[Array[T]]): Array[Array[T]] = {
    val res = Array.ofDim[T](in(0).length, in.length)

    in(0).indices.foreach(y => {
      in.indices.foreach(x => res(y)(x) = in(x)(y))
    })
    res
  }
}
