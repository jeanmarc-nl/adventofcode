package nl.about42.aoc.y2016

import nl.about42.aoc.common.AOCApp

trait AOC2016App extends AOCApp {
  override val year: String = "2016"
  override val resourceDir = "y2016/src/main/resources"
}
