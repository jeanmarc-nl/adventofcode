ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

val circeVersion = "0.14.1"
val sharedLibDeps = Seq(
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "com.typesafe" % "config" % "1.4.2",
  "org.scalacheck" %% "scalacheck" % "1.15.4",
  "org.scalatest" %% "scalatest" % "3.2.9" % "test"
)

lazy val common = (project in file("common"))
  .settings(
    name := "common",
    idePackagePrefix := Some("nl.about42.aoc.common"),
    libraryDependencies ++= sharedLibDeps
  )

lazy val y15 = (project in file("y2015"))
  .settings(
    name := "Year2015",
    idePackagePrefix := Some("nl.about42.aoc.y2015"),
    libraryDependencies ++= sharedLibDeps
  )
  .dependsOn(common)

lazy val y16 = (project in file("y2016"))
  .settings(
    name := "Year2016",
    idePackagePrefix := Some("nl.about42.aoc.y2016"),
    libraryDependencies ++= sharedLibDeps
  )
  .dependsOn(common)

lazy val y17 = (project in file("y2017"))
  .settings(
    name := "Year2017",
    idePackagePrefix := Some("nl.about42.aoc.y2017"),
    libraryDependencies ++= sharedLibDeps
  )
  .dependsOn(common)

lazy val y18 = (project in file("y2018"))
  .settings(
    name := "Year2018",
    idePackagePrefix := Some("nl.about42.aoc.y2018"),
    libraryDependencies ++= sharedLibDeps
  )
  .dependsOn(common)

lazy val y19 = (project in file("y2019"))
  .settings(
    name := "Year2019",
    idePackagePrefix := Some("nl.about42.aoc.y2019"),
    libraryDependencies ++= sharedLibDeps
  )
  .dependsOn(common)

lazy val y20 = (project in file("y2020"))
  .settings(
    name := "Year2020",
    idePackagePrefix := Some("nl.about42.aoc.y2020"),
    libraryDependencies ++= sharedLibDeps ++ Seq(
      "org.scala-lang.modules" %% "scala-parser-combinators" % "2.1.1"
    )
  )
  .dependsOn(common)

lazy val y21 = (project in file("y2021"))
  .settings(
    name := "Year2021",
    idePackagePrefix := Some("nl.about42.aoc.y2021"),
    libraryDependencies ++= sharedLibDeps
  )
  .dependsOn(common)

lazy val root = (project in file("."))
  .settings(
    name := "adventofcode",
    idePackagePrefix := Some("nl.about42.aoc")
  )
  .aggregate(common, y21, y20, y19, y18, y17, y16, y15)
