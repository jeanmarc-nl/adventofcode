package nl.about42.aoc.y2019

import nl.about42.aoc.common.AOCApp

trait AOC2019App extends AOCApp {
  override val year: String = "2019"
  override val resourceDir = "y2019/src/main/resources"
}
